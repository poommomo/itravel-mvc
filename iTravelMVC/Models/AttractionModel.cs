﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iTravelMVC.Models
{
    public class AttractionModel
    {
        public string place { get; set; }
        public string detailShort { get; set; }
        public string detailLong { get; set; }
        public int time { get; set; }
        public string imageUrl { get; set; }
        public string mapUrl { get; set; }

        public AttractionModel(string place, string detailShort, int time, string imageUrl, string detailLong)
        {
            this.place = place;
            this.detailShort = detailShort;
            this.detailLong = detailLong;
            this.time = time;
            this.imageUrl = imageUrl;
        }

    }
}